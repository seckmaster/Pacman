
import java.util.ArrayList;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;


public abstract class Object {
	
	// constructors
	public Object() {}
	
	public Object(int x, int y, float size) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.animators = new ArrayList<>();
	}
	
	public Object(Object o) {
		this.x = o.x;
		this.y = o.y;
		this.size = o.size;
	}
	
	public double manhattanDistance(Object o) {
		double deltaX = x - o.getX();
		double deltaY = y - o.getY();
		return Math.abs(deltaX) + Math.abs(deltaY);
	}
	
	public double euclideanDistance(Object o) {
		return Math.sqrt((o.x - x) * (o.x - x) + (o.y - y) * (o.y - y));
	}
	
	// getters / setters
	public void setSize(float s) {
		size = s;
	}
	
	public float getSize() {
		return size;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setAnimator(Animator a) {
		animators.add(a);
	}
	
	public void setTexture(String file) {
		texture = IO.loadTexture(file);
	}
	
	public void setTexture(Texture t) {
		texture = t;
	}
	
	// abstract methods
	abstract void render(GL2 gl);
	
	// members
	protected int 			x, y; // position
	protected float 		size; // size
	protected Texture		texture;
	protected ArrayList<Animator> animators;
}
