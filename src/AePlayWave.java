import java.io.*;
import javax.sound.sampled.*;

// SOURCE: http://stackoverflow.com/a/6388385
// TODO:
// cache loaded sounds so I can re-use them without loading them over and over again (very slow)
public class AePlayWave implements Runnable {
	AudioInputStream audioInputStream;
	AudioFormat audioFormat;
	
	AePlayWave(String file) {
		File soundFile = new File(file);
        audioInputStream = null;
        try {
            audioInputStream = AudioSystem.getAudioInputStream(soundFile);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        audioFormat = audioInputStream.getFormat();
	}
	
    public void play() {
        Thread t = new Thread(this);
        t.start();
    }
    
    public void run() {
        playSound();
    }
    
    private void playSound() {
        SourceDataLine  line = null;
        DataLine.Info   info = new DataLine.Info(SourceDataLine.class, audioFormat);
        
        try {
            line = (SourceDataLine) AudioSystem.getLine(info);
            line.open(audioFormat);
        }
        catch (LineUnavailableException e) {
            e.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        line.start();
        
        int     nBytesRead = 0;
        byte[]  abData = new byte[128000];
        while (nBytesRead != -1) {
            try {
                nBytesRead = audioInputStream.read(abData, 0, abData.length);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            if (nBytesRead >= 0) {
                int nBytesWritten = line.write(abData, 0, nBytesRead);
            }
        }
        line.drain();
        line.close();
    }
}