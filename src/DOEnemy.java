import java.util.Stack;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;


// http://home.comcast.net/~jpittman2/pacman/pacmandossier.html#Chapter_4
// http://gameinternals.com/post/2072558330/understanding-pac-man-ghost-behavior
// http://gameai.com/wiki/index.php?title=Pac-Man
public abstract class DOEnemy extends DynamicObject {
	
	// constructor
	public DOEnemy(int x, int y, float size) {
		super(x, y, size);
		x_pos = x * size + size / 2;
		y_pos = y * size + size / 2;
		dst_x = x;
		dst_y = y;
		state = E_GHOST_STATE.scatter;
		frightened = IO.loadTexture("resources/textures/frightened.png");
		velocity = 1f;
		changeDirection = true;
	}

	public void setTarget(DynamicObject target) {
		this.target = target; 
	}
	
	public void setState(E_GHOST_STATE s) { 
		state = s; 
		path.clear(); 
		changeDirection = true;
		
		if (s == E_GHOST_STATE.frightened)
			velocity = .5f;
		else
			velocity = 1f;

		// change texture based on state
		texture = state == E_GHOST_STATE.frightened ? frightened : normal;
	}

	public E_GHOST_STATE getState() {
		return state;
	}
	
	public void setWorld(World w) { 
		world = w; 
		solver = new AStar(world); 
	}
	
	public void resetPosition() {
		x = startX; y = startY; 
		x_pos = x * size + size * 0.5; 
		y_pos = y * size + size * 0.5; 
		dst_x = x;
		dst_y = y;
	}
	
	public void startBlinking(boolean p) {
		if (state != E_GHOST_STATE.frightened) return;
		
		if (p) blinkAnimator.start();
		else   blinkAnimator.stop();
	}
	
	@Override
	void render(GL2 gl) {		
		gl.glPushMatrix();
		gl.glLoadIdentity();
	
		//gl.glColor3f(0.5f, 0.5f, 0.5f);
		
		gl.glEnable(GL2.GL_TEXTURE_2D);
		try {
			texture.bind(gl);
		} catch(Exception e) {
			System.err.println("FIle not found: " + e.toString());
		}
		
		gl.glBlendFunc(GL2.GL_ONE, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glColor3f(1f, 1f, 1f);
		gl.glTranslated(x_pos - size / 2, y_pos - size / 2, 0);
		gl.glScalef(size, size, 1);
		
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0.0f, 0.0f);
			gl.glVertex2f(0.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 0.0f);
			gl.glVertex2f(1.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 1.0f);
			gl.glVertex2f(1.0f, 1.0f);
			
			gl.glTexCoord2f(0.0f, 1.0f);
			gl.glVertex2f(0.0f, 1.0f);
		gl.glEnd();
		gl.glPopMatrix();
	}
	
	@Override
	boolean collision(Object o) {		
		if (o == null) return false;
		if (o instanceof OFruit) return false;

		if (x == o.x && y == o.y) return true;
		
		return false;
	}
	
	@Override
	void update(double time) {
		// if not moving, dont do anything
		if (velocity == 0) return;
		
		// when ghost is located at the center of a tile,
		// we move it to the next tile in path
		if (x_pos > x * size + size * 0.5 - velocity &&
			x_pos < x * size + size * 0.5 + velocity &&
			y_pos > y * size + size * 0.5 - velocity &&
			y_pos < y * size + size * 0.5 + velocity) 
		{
			// true if chasing, else false
			boolean p = state == E_GHOST_STATE.chase;
			
			// ghost can change direction only when at intersection (if chasing)
			try {
				if (atInteresction() && p) {
					changeDirection = true;
				}
			}
			catch (ArrayIndexOutOfBoundsException e) {
				System.out.printf("%s - state: %s\n", name, state.toString());
			}
			
			// evaluate ghost's path based on it's state and other variables
			if (changeDirection) {
				// if in chase mode, use AI to determine path
				if (p)
					AI();
				
				// else, go to scatter position
				else if (!p)
					path = solver.solveMaze(x, y, scatterX, scatterY);
				
				changeDirection = false;
			}
			
			// update direction so ghost follows path
			if (!path.isEmpty()) {
				// if not in chase mode, update destination tile only when ..
				// ghost is located on previous destination.
				// this is due to scatterPath not containing full path, but rather path's corners
				if ((!p && (x == dst_x && y == dst_y)) || p) {
					Integer[] a = path.pop();
					dst_x = a[0];
					dst_y = a[1];
				}
				
				// setting ghost's direction based on which tile it should move
				if 		(dst_y > y) direction = 4;
				else if (dst_y < y) direction = 3;
				else if (dst_x < x) direction = 2;
				else if (dst_x > x) direction = 1;
			}

			// if ghost has no more path to follow and is not in chase mode
			// it should follow scatter path
			if (path.isEmpty() && !p) {
				path.addAll(scatterPath);
				changeDirection = false;
			}
		}
		
		if (direction >= 1 && direction <= 4) {
			// move ghost in current direction
			if (direction == 1) x_pos += velocity;
			if (direction == 2) x_pos -= velocity;
			if (direction == 3) y_pos -= velocity;
			if (direction == 4) y_pos += velocity;
			
			// update position
			x = (int) Math.round((x_pos - size / 2) / size);
			y = (int) Math.round((y_pos - size / 2) / size);
		}
		
		for (Animator a : animators)
			a.animate(time, this);
	}
	
	protected abstract void AI();
	
	public enum E_GHOST_STATE {
		halt,
		chase,
		scatter,
		frightened
	}
	
	// return true when ghost is located at intersection
	protected boolean atInteresction() {
		// if in any of 12 corners, return true 
		// (WARNING: this only works for this map)
		if (x == 1  && (y == 1 || y == 18)) return true;
		if (x == 25 && (y == 1 || y == 18)) return true;
		if (x == 6  && (y == 7 || y == 9))  return true;
		if (x == 20 && (y == 7 || y == 9))  return true;
		if (x == 12 && (y == 1 || y == 18)) return true;
		if (x == 14 && (y == 1 || y == 18)) return true;
		
		// ghost is located at intersection when it has at least 3 directions to follow
		int i = 0;
		for (Object o : world.adjacents(x, y)) {
			if (o == null || o instanceof OFruit)
				i++;
		}
		return i >= 3;
	}
	
	// members
	protected String name = "";
	// frightened texture
	protected Texture frightened;
	// normal texture
	protected Texture normal;

	// reference to pacman object
	protected DynamicObject target;
	// state of ghost
	E_GHOST_STATE state;
	// reference to world
	protected World world;
	// path to follow
	protected Stack<Integer[]> path = new Stack<>();
	// destination indeces
	protected int dst_x = -1, dst_y = -1;
	// should ghost change direction
	protected boolean changeDirection;
	// when in scatter mode, tile to go to
	protected int scatterX = -1, scatterY = -1;
	// scatter path so the ghost doesnt stand still when it reaches scatter tile
	protected Stack<Integer[]> scatterPath = new Stack<>();
	// starting position
	protected int startX, startY;
	// maze solver
	protected MazeSolver solver;
	// blink animator
	protected Animator blinkAnimator;
}
