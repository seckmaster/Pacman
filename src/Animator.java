
public abstract class Animator {
	
	public Animator(double sInterval) {
		this.sInterval = sInterval;
		this.stop = true;
	}
	
	public void start() {
		stop = false;
	}
	
	public void stop() {
		stop = true;
	}
	
	public abstract void animate(double time, Object o);
	

	protected double sInterval;
	protected boolean stop;
}
