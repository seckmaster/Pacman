import java.util.ArrayList;

import com.jogamp.opengl.util.texture.Texture;


public class DOEBlinky extends DOEnemy {

	public DOEBlinky(int x, int y, float size) {
		super(x, y, size);
		
		setTexture("resources/textures/blinky.png");
		normal = texture;
		
		ArrayList<Texture> t = new ArrayList<>();
		t.add(normal);
		t.add(frightened);
		blinkAnimator = new ABlinkAnimator(t, 0.5);
		setAnimator(blinkAnimator);
		
		scatterX = 1;
		scatterY = 18;
		scatterPath.add(new Integer[]{1, 18});
		scatterPath.add(new Integer[]{1, 14});
		scatterPath.add(new Integer[]{6, 14});
		scatterPath.add(new Integer[]{6, 18});
		
		startX = x;
		startY = y;
		name = "Blinky";
	}

	@Override
	protected void AI() {
		path = solver.solveMaze(x, y, target.getX(), target.getY());
	}

	
}
