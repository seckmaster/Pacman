import java.util.ArrayList;

import com.jogamp.opengl.util.texture.Texture;


public class DOEInky extends DOEnemy {

	public DOEInky(int x, int y, float size, DynamicObject blinky) {
		super(x, y, size);
		
		setTexture("resources/textures/inky.png");
		normal = texture;
		
		ArrayList<Texture> t = new ArrayList<>();
		t.add(normal);
		t.add(frightened);
		blinkAnimator = new ABlinkAnimator(t, 0.5);
		setAnimator(blinkAnimator);
		
		this.blinky = blinky;
		scatterX = 25;
		scatterY = 18;
		scatterPath.add(new Integer[]{25, 18});
		scatterPath.add(new Integer[]{25, 14});
		scatterPath.add(new Integer[]{20, 14});
		scatterPath.add(new Integer[]{20, 18});
		
		startX = x;
		startY = y;
		
		name = "Inky";
	}
	
	// reference to blinky
	private DynamicObject blinky = null;

	@Override
	protected void AI() {
		int tx = target.getX(), ty = target.getY();
		int td = target.getDirection();
		
		if (td == 2) {
			int d = world.getWidth() - 2 - tx;
			tx += Math.min(2, d);
		}
		else if (td == 1) {
			tx -= Math.min(2, tx - 1);
		}
		else if (td == 3) {
			ty -= Math.min(2, ty - 1);
		}
		else if (td == 4) {
			int d = world.getHeight() - 2 - ty;
			ty += Math.min(2, d);
		}
		
		// vector between blinky and offset tile * 2
		int v_x = blinky.getX() + (tx - blinky.getX()) * 2;
		int v_y = blinky.getY() + (ty - blinky.getY()) * 2;
		
		if (v_x >= world.getWidth() - 1) v_x = world.getWidth() - 2;
		if (v_y >= world.getHeight() - 1) v_y = world.getHeight() - 2;
		if (v_x <= 1) v_x = 2;
		if (v_y <= 1) v_y = 2;
		
		// in case target tile is impassable, dont update the path
		if (world.getObjectAt(v_y, v_x) instanceof OWall) {
			v_x = target.getX();
			v_y = target.getY();
		}
		
		if (v_x >= world.getWidth()  -1 || v_x <= 0 ||
			v_x >= world.getHeight() -1 || v_y <= 0) 
		{
			v_x = target.getX();
			v_y = target.getY();
		}
		
		path = solver.solveMaze(x, y, v_x, v_y);
	}

}
