import java.util.ArrayList;

import com.jogamp.opengl.util.texture.Texture;


public class ABlinkAnimator extends Animator {
	
	public ABlinkAnimator(ArrayList<Texture> textures, double msInterval) {
		super(msInterval);
		this.textures 		= textures;
		this.msLastBlink 	= 0;
		this.previous 		= 0;
	}
	
	@Override
	public void animate(double time, Object o) {
		if (stop) return;
		
		if ((time - msLastBlink) * 0.001 >= sInterval) {
			o.setTexture(textures.get(previous++));
			
			if (previous == textures.size())
				previous = 0;
			
			msLastBlink = time;
		}
	};
	
	private double 				msLastBlink;
	private int 				previous;
	private ArrayList<Texture> 	textures;
}
