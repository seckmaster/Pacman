import com.jogamp.opengl.GL2;


public class OWall extends Object {
	
	public OWall(int x, int y, float size) {
		super(x, y, size);
	}

	@Override
	void render(GL2 gl) {		
		gl.glPushMatrix();
		gl.glLoadIdentity();
		
		//gl.glColor3f(0.5f, 0.5f, 0.5f);
	
		gl.glEnable(GL2.GL_TEXTURE_2D);
		try {
			texture.bind(gl);    
		} catch(Exception e) {
			System.err.println("File not found: " + e.toString());
		}
		
		gl.glBlendFunc(GL2.GL_ONE, GL2.GL_ONE_MINUS_SRC_ALPHA);
		
		gl.glTranslatef(x * size, y * size, 0);
		gl.glColor3f(1f, 1f, 1f);
		gl.glScalef(size, size, 1);
		
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0.0f, 0.0f);
			gl.glVertex2f(0.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 0.0f);
			gl.glVertex2f(1.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 1.0f);
			gl.glVertex2f(1.0f, 1.0f);
			
			gl.glTexCoord2f(0.0f, 1.0f);
			gl.glVertex2f(0.0f, 1.0f);
		gl.glEnd();
	
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glPopMatrix();
	}
	
}
