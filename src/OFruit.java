import java.util.HashMap;
import java.util.Map;

import com.jogamp.opengl.GL2;


public class OFruit extends Object {
	
	private void init_dict() {
		FRUIT_TYPE.put("pac-dot", 10);
		FRUIT_TYPE.put("power-pellet", 50);
		FRUIT_TYPE.put("cherry", 100);
		FRUIT_TYPE.put("strawberry", 300);
		FRUIT_TYPE.put("orange", 500);
		FRUIT_TYPE.put("apple", 700);
		FRUIT_TYPE.put("melon", 1000);
		FRUIT_TYPE.put("galxian boss", 2000);
		FRUIT_TYPE.put("bell", 3000);
		FRUIT_TYPE.put("key", 5000);
		FRUIT_TYPE.put("ghost", 1000);
	}
	
	// constructors
	public OFruit(int x, int y, float size) {
		super(x, y, size);
		init_dict();
		fruit = "pac-dot";
	}
	
	public OFruit(int x, int y, float size, String fruit) {
		this(x, y, size);
		this.fruit = fruit;
	}
	
	// getters / setters
	public String getFruitType() {
		return fruit;
	}
	
	public int getValue() {
		return FRUIT_TYPE.get(fruit);
	}
	
	public static int getValue(String name) {
		return FRUIT_TYPE.get(name);
	}


	@Override
	void render(GL2 gl) {
		gl.glPushMatrix();
		gl.glLoadIdentity();
		
		//gl.glColor3f(0.5f, 0.5f, 0.5f);

		gl.glEnable(GL2.GL_TEXTURE_2D);
		try {
			texture.bind(gl);    
		} catch(Exception e) {
			System.err.println("FIle not found: " + e.toString());
		}
		
		gl.glBlendFunc(GL2.GL_ONE, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glColor3f(1f, 1f, 1f);
		
		gl.glTranslatef(x * size, y * size, 0);
		gl.glScalef(size, size, 1);
		
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0.0f, 0.0f);
			gl.glVertex2f(0.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 0.0f);
			gl.glVertex2f(1.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 1.0f);
			gl.glVertex2f(1.0f, 1.0f);
			
			gl.glTexCoord2f(0.0f, 1.0f);
			gl.glVertex2f(0.0f, 1.0f);
		gl.glEnd();

		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glPopMatrix();
	}
	
	// members
	private static Map<String, Integer> FRUIT_TYPE = new HashMap<String, Integer>();
	private String fruit = "";
}
