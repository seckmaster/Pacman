import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.awt.TextRenderer;


public class GameHandler {
	
	private int width, height;
	public GameHandler(int w, int h) {
		width  = w;
		height = h;
		enableCollision = true;
	}
	
	public void init(GL2 c) {
		context 		= c;
		
    	world 			= new World(27, 20, IO.loadLevel("resources/levels/level1.tmx"));
    	nFruits 		= world.getNumberOfFruits();
    	objects 		= new ArrayList<>();
    	dispose 		= new ArrayList<>();
    	score 			= 0;
    	frightenedTime 	= 8;
    	
    	end 			= false;
    	pause 			= false;
		gameOver 		= false;
    	
    	// scatter chase table based on 
    	// http://home.comcast.net/~jpittman2/pacman/pacmandossier.html#CH2_Scatter_Chase_Repeat
		scatter_chase = new float[][]
		{{7, 20, 7, 20, 5, 20, 5, -1},
		 {7, 20, 7, 20, 5, 1033, 1/60, -1},
		 {5, 20, 5, 20, 5, 1037, 1/60, -1}};

    	// adding default objects into the world
		pacman = new DOPacman(13, 7, 32);
		
    	blinky = new DOEBlinky(13, 12, 32);
    	blinky.setTarget(pacman);
    	blinky.setWorld(world);
    	
    	pinky = new DOEPinky(13, 10, 32);
    	pinky.setTarget(pacman);
    	pinky.setWorld(world);
    	
    	inky = new DOEInky(12, 10, 32, blinky);
    	inky.setTarget(pacman);
    	inky.setWorld(world);
    	
    	clyde = new DOEClyde(14, 10, 32);
    	clyde.setTarget(pacman);
    	clyde.setWorld(world);
    	
    	// load sounds
	}
	
	public void newGame() {
		gameOver 	= false;
		startGame 	= false;
		end 		= false;
		pause 		= false;
		
    	msStartTime = System.currentTimeMillis();
    	msInitialDelay = 2500;
    	
    	timePreviousPhase = msStartTime;
    	timeUntilNextPhase = scatter_chase[currentLevel][0];
    	currentPhase = 0;
    	
    	objects.clear();
		dispose.clear();
    	
		pacman.setPosition(13, 7);
    	pacman.setVelocity(2f);
    	pacman.setDirection(0);
    	objects.add(pacman);
    	
    	blinky.resetPosition();
    	blinky.setState(DOEnemy.E_GHOST_STATE.scatter);
    	objects.add(blinky);

    	pinky.resetPosition();
    	pinky.setState(DOEnemy.E_GHOST_STATE.scatter);
    	objects.add(pinky);

    	inky.resetPosition();
    	inky.setState(DOEnemy.E_GHOST_STATE.scatter);
    	inky.setVelocity(0);
    	objects.add(inky);
    	
    	clyde.resetPosition();
    	clyde.setState(DOEnemy.E_GHOST_STATE.scatter);
    	clyde.setVelocity(0);
    	objects.add(clyde);
	}
	
	public int nextStep() {
		if (!pause && !end && !gameOver)
			msTime = System.currentTimeMillis();
		
		if (msTime - msStartTime >= msInitialDelay && !startGame) {
			startGame 			= true;
			timePreviousPhase 	= msTime;
			msStartTime 		= msTime;
		}
		
		// render static objects
		for (int i = 0; i < world.getHeight(); i++) {
			for (int j = 0; j < world.getWidth(); j++) {
				Object o = world.getObjectAt(i, j);
				if (o != null)
					o.render(context);
			}
		}
		
		// collision handling, updating and rendering dynamic objects
		if (!gameOver && startGame && !pause && !end) {
			for (int i = 0; i < world.getHeight(); i++) {
				for (int j = 0; j < world.getWidth(); j++) {
					Object x = world.getObjectAt(i, j);
					if (x != null && pacman.collision(x))
						handleCollision(pacman, x);
				}
			}
			// handle collisions between pacman and ghosts
			if (blinky.collision(pacman)) 	handleCollision(blinky, pacman);
			if (pinky.collision(pacman)) 	handleCollision(pinky, pacman);
			if (inky.collision(pacman)) 	handleCollision(inky, pacman);
			if (clyde.collision(pacman)) 	handleCollision(clyde, pacman);
		}
		
		// update and render dynamic objects
		for (DynamicObject o : objects) {
			// update objects
			if (!gameOver && startGame && !pause && !end)
				o.update(msTime);
			// render objects
			o.render(context);
		}
		
		// removing eaten fruits from world
		for (Integer[] i : dispose) {
			world.removeObjectAt(i[1], i[0]);
		}
		dispose.clear();
		
		// render texts
		renderer.beginRendering(width, height);
	    renderer.setColor(0, 0, 0, 1);
	    renderer.draw("Score: " + score, 350, 650);
	    renderer.draw("Lives: " + lives, 720, 650);
	    renderer.draw("Level: " + currentLevel, 0, 650);
	    if (gameOver) {
	    	if (nFruits == 0)
		    	renderer.draw("You won!", 280, 450);
	    	else
	    		renderer.draw("Game over, you lost!", 280, 450);
	    }
	    else if (!startGame)
	    	renderer.draw("Get ready!", 350, 450);
	    else if (pause)
	    	renderer.draw("Pause", 350, 450);
	    renderer.endRendering();
	    

	    if (gameOver || !startGame || pause || end) return 0;
		
		// game logic related stuff
	    
	    // change phase
		if ((msTime - timePreviousPhase) * 0.001 >= timeUntilNextPhase && 
			timeUntilNextPhase != -1 && msFrightenedStart == -1) {
			if (currentPhase < scatter_chase[currentLevel].length - 1)
				timeUntilNextPhase = scatter_chase[currentLevel][++currentPhase];
			
			DOEnemy.E_GHOST_STATE s = DOEnemy.E_GHOST_STATE.halt;
			
			if (currentPhase % 2 == 0) 	s = DOEnemy.E_GHOST_STATE.scatter;
			else 						s = DOEnemy.E_GHOST_STATE.chase;
			
			blinky.setState(s);
			pinky.setState(s);
			inky.setState(s);
			clyde.setState(s);
			
			timePreviousPhase = msTime;
		}
		
		// should inky and clyde start chasing?
		double l = (msTime - msStartTime) * 0.001;
		if (l >= inky_delay)
			inky.setVelocity(1f);
		if (l >= clyde_delay)
			clyde.setVelocity(1f);
		
		// if ghosts are frightened
		if (msFrightenedStart != -1) {
			double delta = (msTime - msFrightenedStart) * 0.001;
			// check if time expired
			if (delta >= frightenedTime) {
				// stop blinking
				blinky.startBlinking(false);
				inky.startBlinking(false);
				pinky.startBlinking(false);
				clyde.startBlinking(false);
				
				// start chasing
				blinky.setState(DOEnemy.E_GHOST_STATE.chase);
				pinky.setState(DOEnemy.E_GHOST_STATE.chase);
				inky.setState(DOEnemy.E_GHOST_STATE.chase);
				clyde.setState(DOEnemy.E_GHOST_STATE.chase);
				
				msFrightenedStart = -1;
			}
			// if only 3 seconds are left, ghosts start blinking
			else if (delta >= frightenedTime - 3) {
				blinky.startBlinking(true);
				inky.startBlinking(true);
				pinky.startBlinking(true);
				clyde.startBlinking(true);
			}
		}
		
		return 0;
	}
	
	private void handleCollision(Object a, Object b) {
		
		if (a instanceof DOPacman) {
			if (b instanceof OWall) {
				((DynamicObject) a).setDirection(0);
			}
			else if (b instanceof OFruit) {
				score += ((OFruit) b).getValue();
				nFruits--;
				dispose.add(new Integer[]{b.getX(), b.getY()});
				
				if (((OFruit) b).getFruitType().equals("power-pellet")) {
					blinky.setState(DOEnemy.E_GHOST_STATE.frightened);
					inky  .setState(DOEnemy.E_GHOST_STATE.frightened);
					pinky .setState(DOEnemy.E_GHOST_STATE.frightened);
					clyde .setState(DOEnemy.E_GHOST_STATE.frightened);
					msFrightenedStart = msTime;
				}
				
				if (nFruits == 0) {
					currentLevel++;
					gameOver = true;
					newGame();
				}
				
				// play sound for eating fruit
				// TODO
			}
		}
		else {
			// if collisions are disabled, dont do anything
			if (!enableCollision) return;
			
			if (b instanceof DOPacman) {
				if (((DOEnemy) a).getState() == DOEnemy.E_GHOST_STATE.frightened) {
					score += OFruit.getValue("ghost");
					((DOEnemy) a).resetPosition();
					((DOEnemy) a).setState(DOEnemy.E_GHOST_STATE.chase);
				}
				else {
					end = true;
					lives--;
					
					if (lives == 0) {
						gameOver = true;
						//cleanUp();
					}
					else
						newGame();
				}
			}
		}
	}
	
	private void cleanUp() {
		objects.clear();
		dispose.clear();
		objects 			= null;
		dispose 			= null;
		blinky 				= null;
		inky 				= null;
		pinky 				= null;
		clyde 				= null;
		renderer 			= null;
		pacman 				= null;
		world 				= null;
		scatter_chase 		= null;
	}
	
	public void handleKeyPres(KeyEvent e) {
		char c = e.getKeyChar();
		if 		(c == 'w') pacman.setDirection(4);
		else if (c == 's') pacman.setDirection(3);
		else if (c == 'a') pacman.setDirection(1);
		else if (c == 'd') pacman.setDirection(2);
		else if (c == 'k') pacman.setDirection(0);
		else if (c == 'p') pause = !pause;
		else if (c == '1') enableCollision = true;
		else if (c == '2') enableCollision = false;
	}
	
	// members
	
	// sounds
	/*private AePlayWave 						move;
	private AePlayWave 						death;
	private AePlayWave 						eatGhost;
	private AePlayWave 						eatFruit;*/
	
	private DOPacman	 					pacman;
	private DOEnemy							blinky;
	private DOEnemy							inky;
	private DOEnemy							pinky;
	private DOEnemy							clyde;
	private int 							inky_delay 	= 4; 		// delay in seconds
	private int 							clyde_delay = 8;		// delay in seconds
	
	private ArrayList<DynamicObject>  		objects;  				// dynamic objects
	private World 							world;					// static objects
	private ArrayList<Integer[]>			dispose;				// objects to destroy
	private boolean 						enableCollision;		// enable / disable collisions between pacman and enemies
	
    private TextRenderer 					renderer = 
    								new TextRenderer(new Font("SansSerif", Font.BOLD, 36));
	
	private GL2 							context;
	
	// game logic related members
	private long 							msStartTime;			// time when game launched
	private long							msTime; 				// current time in miliseconds
	private int 							msInitialDelay; 		// game doesnt start immidietly, but after delay
	private boolean							startGame;				// true after initial delay
	
	private int 							score;
	private int 							nFruits;				// how many fruits left to eat
	private int 							currentLevel = 1;		// current level
	private int 							lives = 3;				// number of pacman's lives
	private float[][]						scatter_chase; 			// scatter-chase table for all levels
	private float 							frightenedTime; 		// for how long are ghosts in frightened mode
	private long							msFrightenedStart = -1;	// time when ghosts became frightened 
	private int 							currentPhase = 0; 		// ghosts are in scatter phase first
	private float 							timeUntilNextPhase; 	// time unitl next phase
	private long							timePreviousPhase; 		// when did the previous phase start
	
	public boolean							gameOver;				// ran out of lives
	private boolean							pause;					// pause
	private boolean 						end; 					// true when ghost ate pacman
}
