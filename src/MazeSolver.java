import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Stack;

// http://en.wikipedia.org/wiki/Maze_solving_algorithm
// SOURCE: http://www.laurentluce.com/posts/solving-mazes-using-python-simple-recursivity-and-a-search/

public interface MazeSolver {
	abstract Stack<Integer[]> solveMaze(int src_x, int src_y, int dst_x, int dst_y);
	abstract void reset();
}

class AStar implements MazeSolver {

	@Override
	public Stack<Integer[]> solveMaze(int src_x, int src_y, int dst_x, int dst_y) {
		this.dst_x = dst_x;
		this.dst_y = dst_y;
		this.src_x = src_x;
		this.src_y = src_y;
		
		solve();
		reset();
		return reconstructPath();
	}
	
	@Override
	public void reset() {
		closed.clear();
		opened.clear();
	}
	
	class Cell {
		boolean reachable = false;
		int x, y;
		Cell parent = null;
		int g = 0, h = 0, f = 0;
		
		Cell(int x, int y, boolean r) {
			reachable = r;
			this.x = x;
			this.y = y;
		}
	}

	public AStar(World w) {
		width = w.getWidth();
		height = w.getHeight();
		maze = new Cell[height][width];
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (w.getObjectAt(i, j) == null || w.getObjectAt(i, j) instanceof OFruit)
					maze[i][j] = new Cell(j, i, true); // path
				else
					maze[i][j] = new Cell(j, i, false); // wall
			}
		}
		maze[11][12] = new Cell(12, 11, true);
		maze[11][13] = new Cell(13, 11, true);
		maze[11][14] = new Cell(14, 11, true);
		maze[12][12] = new Cell(12, 12, true);
		maze[12][13] = new Cell(13, 12, true);
		maze[12][14] = new Cell(14, 12, true);
	}
	
	private int heuristic(Cell src) {
		return 10 * (Math.abs(src.x - dst_x) + Math.abs(src.y - dst_y));
	}
	
	private ArrayList<Cell> adjacentCells(Cell c) {
		try {
			int x = c.x, y = c.y;
			ArrayList<Cell> adj = new ArrayList<>();
			
			if (c.x < width - 1)	adj.add(maze[y+1][x]);
			if (c.x > 0) 			adj.add(maze[y-1][x]);
			if (c.y < width - 1) 	adj.add(maze[y][x+1]);
			if (c.y > 0) 			adj.add(maze[y][x-1]);
			
			return adj;
		}
		catch (IndexOutOfBoundsException e) {
			return new ArrayList<>();
		}
	}
	
	private void updateCell(Cell c, Cell adj) {
		adj.g = 10 + c.g;
		adj.h = heuristic(adj);
		adj.parent = c;
		adj.f = adj.h + adj.g;
	}
	
	private void solve() {
		opened.add(new Cell(src_x, src_y, true));
		
		while (!opened.isEmpty()) {
			Cell poped = opened.poll();
			closed.add(poped);
			if (poped.x == dst_x && poped.y == dst_y)
				return;
			
			ArrayList<Cell> adjacent = adjacentCells(poped);
			for (Cell c : adjacent) {
				if (c.reachable && !closed.contains(c)) {
					if (opened.contains(c)) {
						if (c.g > poped.g + 10)
							updateCell(poped, c);
					}
					else {
						updateCell(poped, c);
						opened.add(c);
					}
				}
			}
		}
	}
	
	private Stack<Integer[]> reconstructPath() {
		Stack<Integer[]> path = new Stack<>();
		try {
			Cell c = maze[dst_y][dst_x];
			while (c.x != src_x || c.y != src_y) {
				path.push(new Integer[]{c.x, c.y});
				c = maze[c.parent.y][c.parent.x];
			}
		}
		catch (Exception e) {
			System.out.printf("Exception at MazeSolver:\nDst x: %d Dst y: %d Src x: %d Src y: %d error: %s \n", dst_x, dst_y, src_x, src_y, e.toString());
		}
		return path;
	}
	
	private int width, height;
	private int dst_x, dst_y;
	private int src_x, src_y;
	private Cell[][] maze;
	private ArrayList<Cell> closed = new ArrayList<>();
	private PriorityQueue<Cell> opened = new PriorityQueue<Cell>(new Comparator<Cell>() {

		@Override
		public int compare(Cell o1, Cell o2) {
			if (o1.f < o2.f) return -1;
			if (o1.f > o2.f) return 1;
			return 0;
		}
		
	});
}
