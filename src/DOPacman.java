import com.jogamp.opengl.GL2;


public class DOPacman extends DynamicObject {
	
	public DOPacman(int x, int y, float size) { 
		super(x, y, size); 
		x_pos = x * size + size / 2;
		y_pos = y * size + size / 2;
    	setTexture("resources/textures/pacman.png");
	}
	
	// getters / setters


	@Override
	void render(GL2 gl) {
		gl.glPushMatrix();
		gl.glLoadIdentity();

		//gl.glColor3f(0.5f, 0.5f, 0.5f);
		
		gl.glEnable(GL2.GL_TEXTURE_2D);
		try {
			texture.bind(gl);    
		} catch(Exception e) {
			System.err.println("FIle not found: " + e.toString());
		}
		
		gl.glBlendFunc(GL2.GL_ONE, GL2.GL_ONE_MINUS_SRC_ALPHA);
		gl.glColor3f(1f, 1f, 1f);
		gl.glTranslated(x_pos - size / 2, y_pos - size / 2, 0);
		gl.glScalef(size, size, 1);
		
		gl.glBegin(GL2.GL_QUADS);
			gl.glTexCoord2f(0.0f, 0.0f);
			gl.glVertex2f(0.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 0.0f);
			gl.glVertex2f(1.0f, 0.0f);
			
			gl.glTexCoord2f(1.0f, 1.0f);
			gl.glVertex2f(1.0f, 1.0f);
			
			gl.glTexCoord2f(0.0f, 1.0f);
			gl.glVertex2f(0.0f, 1.0f);
		gl.glEnd();
		
		gl.glDisable(GL2.GL_TEXTURE_2D);
		gl.glPopMatrix();
	}

	@Override
	boolean collision(Object o) {
		float hs = o.getSize() / 2;
		float a = o.getX() * size + hs;
		float b = o.getY() * size + hs;
		
		if (o instanceof OFruit) {
			if (x == o.x && y == o.y) return true;
			return false;
		}
			

		double mhs = size / 2; // my half-size
		double dx = x_pos, dy = y_pos; // desired x y
		
		if 		(direction == 2) dx += velocity;
		else if (direction == 1) dx -= velocity;
		else if (direction == 4) dy += velocity;
		else if (direction == 3) dy -= velocity;
		
		double collisionX1 = (dx + mhs) - (a - hs);
		double collisionX2 = (dx - mhs) - (a + hs);
		double collisionY1 = (dy + mhs) - (b - hs);
		double collisionY2 = (dy - mhs) - (b + hs);
		
		float offset = size / 2;
		
		// collision detection + corner cutting
		if ((collisionX1 > 0 && collisionX2 < 0) && 
			(collisionY1 > 0 && collisionY2 < 0)) {
			// bottom - left corner collision
			if (collisionX1 < offset && collisionY1 < offset) {
				if (direction == 2) {
					y_pos -= collisionY1;
				}				
				else if (direction == 4) {
					x_pos -= collisionX1;
				}
				return false;
			}
			// top - left corner collision			
			if (collisionX1 < offset && collisionY2 > -offset) {
				if (direction == 2) {
					y_pos -= collisionY2;
				}				
				else if (direction == 3) {
					x_pos -= collisionX1;
				}
				return false;
			}
			// bottom - right corner collision
			if (collisionX2 > -offset && collisionY1 < offset) {
				if (direction == 1) {
					y_pos -= collisionY1;
				}				
				else if (direction == 4) {
					x_pos -= collisionX2;
				}
				return false;
			}
			// top - right corner collision			
			if (collisionX2 > -offset && collisionY2 > -offset) {
				if (direction == 1) {
					y_pos -= collisionY2;
				}				
				else if (direction == 3) {
					x_pos -= collisionX2;
				}
				return false;
			}
				
			return true;
		}
		
		return false;
	}

	@Override
	void update(double time) {
		if (direction < 1 || direction > 4) return;
		
		if (direction == 1) x_pos -= velocity;
		if (direction == 2) x_pos += velocity;
		if (direction == 3) y_pos -= velocity;
		if (direction == 4) y_pos += velocity;
		
		x = (int) Math.round((x_pos - size / 2) / size);
		y = (int) Math.round((y_pos - size / 2) / size);
	}
	
	
}
