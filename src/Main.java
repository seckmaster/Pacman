import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.LayoutManager;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;


public class Main {
	
	static class JPanelBG extends JPanel {

		  private Image img;

		  public JPanelBG(String img) {
			  this(new ImageIcon(img).getImage());
		  }

		  public JPanelBG(Image img) {
				this.img = img;
				Dimension size = new Dimension(img.getWidth(null), img.getHeight(null));
				setPreferredSize(size);
				setMinimumSize(size);
				setMaximumSize(size);
				setSize(size);
				setLayout(null);
		  }

		  public void paintComponent(Graphics g) {
		    g.drawImage(img, 0, 0, null);
		  }

		}
	
    public static void main(String[] args) {
        GLProfile glprofile = GLProfile.getDefault();
        GLCapabilities glcapabilities = new GLCapabilities(glprofile);
        final GLCanvas glcanvas = new GLCanvas(glcapabilities);
        FPSAnimator animator = new FPSAnimator(glcanvas, 60);
        
        int width = 864;
        int height = 720;
        
        GameHandler pacman = new GameHandler(width, height);
        
        glcanvas.addGLEventListener(new GLEventListener() {
            
            @Override
            public void reshape(GLAutoDrawable glautodrawable, int x, int y, int width, int height ) {
            	GL2 gl = glautodrawable.getGL().getGL2();
            }
            
            @Override
            public void init(GLAutoDrawable glautodrawable ) {
            	GL2 gl = glautodrawable.getGL().getGL2();
            	
            	gl.glMatrixMode(GL2.GL_PROJECTION);
            	gl.glLoadIdentity();
            	gl.glOrtho(0.0, width, 0.0, height, -1, 1);
            	
            	gl.glMatrixMode(GL2.GL_MODELVIEW);
            	gl.glLoadIdentity();
            	gl.glClearColor(1, 1, 1, 1);
            	gl.glViewport(0, 0, width, height);
            	
            	gl.glDisable(GL2.GL_DEPTH_TEST);
            	gl.glDisable(GL2.GL_LIGHTING);
            	gl.glEnable(GL2.GL_BLEND);
            	
                pacman.init(gl);
                pacman.newGame();
                animator.setUpdateFPSFrames(3, null);
            }
            
            @Override
            public void dispose(GLAutoDrawable glautodrawable ) {
            	
            }
            
            @Override
            public void display(GLAutoDrawable glautodrawable ) {
            	GL2 gl = glautodrawable.getGL().getGL2();
            	
            	gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
        		if (pacman.nextStep() != 0) {
        			System.exit(0);
        		}
            }
        });
        
        glcanvas.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				animator.start();
				pacman.handleKeyPres(e);
			}
		});

        JFrame jframe = new JFrame("Pacman :)");
        jframe.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowevent) {
                jframe.dispose();
                animator.stop();
                System.exit(0);
            }
        });
        jframe.setSize(width, height);
        
        JPanel jpanel = new JPanelBG(new ImageIcon("resources/textures/bg.png").getImage());
        GroupLayout layout = new GroupLayout(jpanel);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        jpanel.setLayout(layout);
        
        jpanel.setVisible(true);
        jframe.add(jpanel);
        
        JLabel l = new JLabel("WELCOME TO PACMAN");
        l.setSize(400, 150);
        l.setForeground(Color.white);
        l.setFont(new Font(l.getFont().getName(), Font.BOLD, 32));
        l.setLocation(width / 2 - l.getWidth() / 2, -45);
        jpanel.add(l);
        
        JButton b = new JButton("New game");
        b.setSize(300, 75);
        b.setFont(new Font(b.getFont().getName(), Font.BOLD, 18));
        b.setLocation(width / 2 - b.getWidth() / 2, 80);
        b.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				jpanel.setVisible(false);
				jframe.remove(jpanel);
				jpanel.removeAll();
				
				// start the game
		        jframe.add(glcanvas, BorderLayout.CENTER);
				glcanvas.setVisible(true);
				glcanvas.requestFocus();
		        animator.start();
			}
		});
        jpanel.add(b);
        

        b = new JButton("Exit!");
        b.setSize(300, 75);
        b.setFont(new Font(b.getFont().getName(), Font.BOLD, 18));
        b.setLocation(width / 2 - b.getWidth() / 2, 200);
        b.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
        jpanel.add(b);
        
        l = new JLabel("<html>Instructions: movement - WASD<br>pause: p<br>enable / disable collisions - 1 / 2</html>");
        l.setSize(400, 800);
        l.setForeground(Color.white);
        l.setFont(new Font(l.getFont().getName(), Font.BOLD, 26));
        l.setLocation(50, 150);
        jpanel.add(l);
        
        jframe.setVisible(true);
        
        // play main menu sound
    	new AePlayWave("resources/sounds/pacman_beginning.wav").play();
    }
    
}


