import java.util.ArrayList;

import com.jogamp.opengl.util.texture.Texture;


public class DOEClyde extends DOEnemy {

	public DOEClyde(int x, int y, float size) {
		super(x, y, size);
	
		setTexture("resources/textures/clyde.png");
		normal = texture;
		
		ArrayList<Texture> t = new ArrayList<>();
		t.add(normal);
		t.add(frightened);
		blinkAnimator = new ABlinkAnimator(t, 0.5);
		setAnimator(blinkAnimator);
		
		scatterX = 1;
		scatterY = 1;
		scatterPath.add(new Integer[]{1, 1});
		scatterPath.add(new Integer[]{1, 5});
		scatterPath.add(new Integer[]{6, 5});
		scatterPath.add(new Integer[]{6, 1});
		
		startX = x;
		startY = y;
		
		name = "Clyde";
	}

	@Override
	protected void AI() {
		int tx = 0, ty = 0;
		
		if (euclideanDistance(target) >= 8) {
			tx = target.getX();
			ty = target.getY();
		}
		else {
			tx = scatterX;
			ty = scatterY;
		}

		path = solver.solveMaze(x, y, tx, ty);
	}

}
