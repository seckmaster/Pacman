import java.util.ArrayList;

import com.jogamp.opengl.util.texture.Texture;


public class DOEPinky extends DOEnemy {

	public DOEPinky(int x, int y, float size) {
		super(x, y, size);
		
		setTexture("resources/textures/pinky.png");
		normal = texture;
		
		ArrayList<Texture> t = new ArrayList<>();
		t.add(normal);
		t.add(frightened);
		blinkAnimator = new ABlinkAnimator(t, 0.5);
		setAnimator(blinkAnimator);
		
		scatterX = 25;
		scatterY = 1;
		scatterPath.add(new Integer[]{25, 1});
		scatterPath.add(new Integer[]{20, 1});
		scatterPath.add(new Integer[]{20, 5});
		scatterPath.add(new Integer[]{25, 5});
		
		startX = x;
		startY = y;
		
		name = "Pinky";
	}

	@Override
	protected void AI() {
		int tx = target.getX(), ty = target.getY();
		int td = target.getDirection();
		
		if (td == 2) {
			int d = world.getWidth() - 2 - tx;
			tx += Math.min(4, d);
		}
		else if (td == 1) {
			tx -= Math.min(4, tx - 1);
		}
		else if (td == 3) {
			ty -= Math.min(4, ty - 1);
		}
		else if (td == 4) {
			int d = world.getHeight() - 2 - ty;
			ty += Math.min(4, d);
		}
		
		// in case target tile is impassable, dont update the path
		if (world.getObjectAt(ty, tx) instanceof OWall) {
			tx = target.getX();
			ty = target.getY();
		}
		
		if (tx >= world.getWidth()  -1 || tx <= 0 ||
			ty >= world.getHeight() -1 || ty <= 0) 
		{
			tx = target.getX();
			ty = target.getY();
		}
		
		path = solver.solveMaze(x, y, tx, ty);
	}

}
