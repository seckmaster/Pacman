import java.io.File;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;


public class IO {
	
	public static Texture loadTexture(String file) {
		try {
			return TextureIO.newTexture(new File(file), false);
		} catch (Exception e) {
			System.err.println("Loader!" + e.toString());
		}
		
		return null;
	}
	
	public static ArrayList<Object> loadLevel(String file) {
		try {
			File fXmlFile = new File(file);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();
		 
			NodeList nList = doc.getElementsByTagName("data");
			nList = nList.item(0).getChildNodes();
			
			ArrayList<Object> world = new ArrayList<>();
			
			int a = 0, b = 0;
			for (int i = 0; i < nList.getLength(); i++) {
				Node n = nList.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					Element e = (Element) n;
					int gid = Integer.parseInt(e.getAttribute("gid"));
					int size = 32;
					
					if (gid == 1) {
						Object o = new OWall(b, 19 - a, size);
						o.setTexture("resources/textures/wall.png");
						world.add(o);
					}
					else if (gid == 2) {
						Object o = new OFruit(b, 19 - a, size, "pac-dot");
						o.setTexture("resources/textures/pac-dot.png");
						world.add(o);
					}
					else if (gid == 3) {
						Object o = new OFruit(b, 19 - a, size, "power-pellet");
						o.setTexture("resources/textures/power-pellet.png");
						world.add(o);
					}
					else {
						Object o = new OWall(b, 19 - a, size);
						o.setTexture("resources/textures/wall.png");
						world.add(o);
					}
					
					b++;
					if (b == 27) {
						b = 0;
						a++;
					}
				}
			}
			
			return world;
		}
		catch (Exception e) {
			System.err.println(e.toString());
		}
		
		return null;
	}

}
