
public abstract class DynamicObject extends Object {
	
	public DynamicObject(int x, int y, float size) { 
		super(x, y, size); 
		x_pos = x * size;
		y_pos = y * size;
		velocity = 0;
	}
	
	abstract void update(double time);
	abstract boolean collision(Object o);
	
	// getters / setters
	public void setVelocity(float v) 		{ velocity = v; }
	public void setDirection(int d) 		{ direction = d; }
	
	public float getVecolity() 				{ return velocity; }
	public int getDirection() 				{ return direction; }

	public double getPosX() 				{ return x_pos; }
	public double getPosY() 				{ return y_pos; }
	
	public void setPosition(int i, int j) 	{
		x = i; y = j; 
		x_pos = x * size + size * 0.5; 
		y_pos = y * size + size * 0.5; 
	}
	
	// members
	protected float 					velocity;
	protected int 						direction;
	protected double 					x_pos, y_pos;
}
