import java.util.ArrayList;

public class World {
	
	public World(int width, int height, ArrayList<Object> data) {
		this.width 		= width;
		this.height 	= height;
		nOfFruits 		= 0;
		
		grid = new Object[height][width];
		for (Object o : data)
		{
			grid[o.getY()][o.getX()] = o;
			if (o instanceof OFruit)
				nOfFruits++;
		}
	}
	
	// return object at position (i, j)
	public Object getObjectAt(int i, int j) {
		if (i < 0 || i >= height || j < 0 || j >= width)
			return null;
		return grid[i][j];
	}
	
	// return list od adjacent objects
	public ArrayList<Object> adjacents(int x, int y) {
		ArrayList<Object> adj = new ArrayList<>();
		if (x < width - 1)	adj.add(grid[y+1][x]);
		if (x > 0) 			adj.add(grid[y-1][x]);
		if (y < width - 1) 	adj.add(grid[y][x+1]);
		if (y > 0) 			adj.add(grid[y][x-1]);
		return adj;
	}

	// getters / setters
	public int 		getWidth() { return width; }
	public int 		getHeight() { return height; }
	public void 	removeObjectAt(int i, int j) { grid[i][j] = null; }
	public int 		getNumberOfFruits() { return nOfFruits; }
	
	private int 					width;
	private int 					height;
	private Object[][] 				grid;
	private int						nOfFruits;
}
